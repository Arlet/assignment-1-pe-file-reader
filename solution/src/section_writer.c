#include "section_writer.h"

/// @file
/// @brief File for realization writeSection()

/// @brief Size of buffer for read/write operations for section
#define BUFFER_SIZE 1024

/// @brief Write a section in output file
/// @param[in] readingFile File that use for reading the section
/// @param[in] writingFile File where will write the section
/// @param[in] peFile Prepared PE file for getting address
/// @param[in] sectionIndex Index in peFile of section
enum writeStatus writeSection(FILE *readingFile, FILE *writingFile, struct PEFile peFile, size_t sectionIndex)
{
    struct SectionHeader currentHeader = peFile.sectionHeaders[sectionIndex];

    if (fseek(readingFile, currentHeader.pointerToRawData, SEEK_SET) != 0)
        return SECTION_READ_ERROR;

    uint8_t buff[BUFFER_SIZE];

    for (size_t i = 0; i < currentHeader.sizeOfRawData / BUFFER_SIZE; i++)
    {
        if (fread(buff, sizeof(uint8_t), BUFFER_SIZE, readingFile) == 0)
            return SECTION_READ_ERROR;

        if (fwrite(buff, sizeof(uint8_t), BUFFER_SIZE, writingFile) == 0)
            return WRITE_ERROR;
    }

    if (currentHeader.sizeOfRawData % BUFFER_SIZE != 0)
    {
        if (fread(buff, sizeof(uint8_t), currentHeader.sizeOfRawData % BUFFER_SIZE, readingFile) == 0)
            return SECTION_READ_ERROR;

        if (fwrite(buff, sizeof(uint8_t), currentHeader.sizeOfRawData % BUFFER_SIZE, writingFile) == 0)
            return WRITE_ERROR;
    }

    return WRITE_SUCCESS;
}
