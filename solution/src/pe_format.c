#include "pe_format.h"

/// @file
/// @brief File with realization of allocateSectionHeaders() and freePEFile()

/// @brief Allocate memory for section headers
/// @param[in] count Size of an array of section headers
/// @return Pointer to an allocated memory for an array of section headers (can be NULL if allocation got fault)
struct SectionHeader *allocateSectionHeaders(size_t count)
{
    struct SectionHeader *header = malloc(sizeof(struct SectionHeader) * count);

    return header;
}

/// @brief Free allocated memory from PEFile
/// @param[in] file File that need to be free
void freePEFile(struct PEFile file)
{
    if (file.sectionHeaders != NULL) {
        free(file.sectionHeaders);
        file.sectionHeaders = NULL;
    }
}
