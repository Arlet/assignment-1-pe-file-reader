/// @file
/// @brief Main application file

#include "pe_reader.h"
#include "section_writer.h"
#include <stdio.h>
#include <string.h>

/// Application name string
#define APP_NAME "section-extractor"
#define NOT_ENOUGH_ARGS_CODE 1
#define FILE_NOT_FOUND_CODE 2
#define FILE_READING_CODE 3
#define INCORRECT_SIGNATURE_CODE 4
#define WRITE_SECTION_CODE 5
#define READ_SECTION_CODE 6
#define MEMORY_ALLOCATION_CODE 7

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
	fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code (see macros for this file)
int main(int argc, char **argv)
{
	if (argc != 4)
	{
		usage(stdout);
		return NOT_ENOUGH_ARGS_CODE;
	}

	FILE *file = fopen(argv[1], "rb");

	if (file == NULL)
	{
		fprintf(stderr, "File not found\n");
		return FILE_NOT_FOUND_CODE;
	}

	struct PEFile parsedPE;

	switch (readPEFile(file, &parsedPE))
	{
	case FILE_READ_ERROR:
		fprintf(stderr, "Error with file reading\n");
		fclose(file);
		return FILE_READING_CODE;
	case WRONG_SIGNATURE:
		fprintf(stderr, "It's not PE file\n");
		fclose(file);
		return INCORRECT_SIGNATURE_CODE;
	case MEMORY_ALLOCATION_ERROR:
		fprintf(stderr, "Error memory allocation\n");
		fclose(file);
		return MEMORY_ALLOCATION_CODE;
	default:
		break;
	}

	size_t sectionIndex = -1;

	for (int i = 0; i < parsedPE.header.numberOfSections; i++)
	{
		if (strncmp(parsedPE.sectionHeaders[i].name, argv[2], 8) == 0)
		{
			sectionIndex = i;
			break;
		}
	}

	if (sectionIndex == -1)
	{
		printf("Section not found\n");
		fclose(file);
		freePEFile(parsedPE);
		return 0;
	}

	FILE *output = fopen(argv[3], "wb");

	if (output == NULL)
	{
		fprintf(stderr, "Error with output file opening\n");
		fclose(file);
		freePEFile(parsedPE);
		return FILE_NOT_FOUND_CODE;
	}

	switch (writeSection(file, output, parsedPE, sectionIndex))
	{
	case WRITE_ERROR:
		fprintf(stderr, "Error with writing section in file\n");
		fclose(file);
		fclose(output);
		freePEFile(parsedPE);
		return WRITE_SECTION_CODE;
	case SECTION_READ_ERROR:
		fprintf(stderr, "Error with reading section\n");
		fclose(file);
		fclose(output);
		freePEFile(parsedPE);
		return READ_SECTION_CODE;
	default:
		break;
	}

	printf("Success.\n");

	fclose(file);
	fclose(output);
	freePEFile(parsedPE);

	return 0;
}
