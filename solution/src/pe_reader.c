#include "pe_reader.h"
#include <inttypes.h>
/// @file
/// @brief File with realization of readPEFile()

/// @brief Read PE headers
/// @param[in] file PE File that need to read
/// @param[out] peFile Pointer to variable for save result
/// @return Some read status (more in readStatus)
enum readStatus readPEFile(FILE *file, struct PEFile *peFile)
{
    if (fseek(file, 0x3C, SEEK_SET) != 0)
        return FILE_READ_ERROR;

    uint32_t offset;

    if (fread(&offset, sizeof(uint32_t), 1, file) == 0)
        return FILE_READ_ERROR;

    if (fseek(file, offset, SEEK_SET) != 0)
        return FILE_READ_ERROR;

    uint32_t magic;

    if (fread(&magic, sizeof(uint32_t), 1, file) == 0)
        return FILE_READ_ERROR;

    if (magic != 0x4550) // PE
        return WRONG_SIGNATURE;

    struct PEHeader header;
    struct OptionalHeader optionalHeader;

    if (fread(&header, sizeof(struct PEHeader), 1, file) == 0)
        return FILE_READ_ERROR;

    if (fread(&optionalHeader, sizeof(struct OptionalHeader), 1, file) == 0)
        return FILE_READ_ERROR;

    struct SectionHeader *sectionHeaders = allocateSectionHeaders(header.numberOfSections);

    if (sectionHeaders == NULL)
        return MEMORY_ALLOCATION_ERROR;

    if (fread(sectionHeaders, sizeof(struct SectionHeader), header.numberOfSections, file) == 0) {
        free(sectionHeaders);
        return FILE_READ_ERROR;
    }

    *peFile = (struct PEFile){.header = header, .optionalHeader = optionalHeader, .sectionHeaders = sectionHeaders};

    return READ_SUCCESS;
}
