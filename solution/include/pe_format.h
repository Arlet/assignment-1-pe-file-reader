#ifndef PE_FORMAT_H
#define PE_FORMAT_H

#include <malloc.h>
#include <stdint.h>

/// @file
/// @brief Header file for all structres in a PE file

struct
#ifdef _MSC_VER
#pragma packed()
#else
    __attribute__((packed))
#endif
    PEHeader
{
    /// @name Main header of a PE file
    ///@{
    ///< Processor ID
    uint16_t machine;
    ///< Number of sections in the file
    uint16_t numberOfSections;
    ///< Date of creation (unix time)
    uint32_t timeDateStamp;
    ///< Raw pointer to the symbol table
    uint32_t pointerToSymbolTable;
    ///< Number of elements in the symbol table
    uint32_t numberOfSymbols;
    ///< Size of an optional header
    uint16_t sizeOfOptionalHeader;
    ///< Atributes of the file
    uint16_t characterstics;

    ///@}
};

struct
#ifdef _MSC_VER
#pragma packed()
#else
    __attribute__((packed))
#endif
    DataDirectory
{
    /// @name Structure for Data directories in an Optional header
    uint32_t virtualAddress;
    uint32_t size;
};

struct
#ifdef _MSC_VER
#pragma packed()
#else
    __attribute__((packed))
#endif
    OptionalHeader
{
    /// @name Optional header for PE32+
    /// @{
    ///< Signature of an optional header
    uint16_t magic;
    uint8_t majorLinkerVersion;
    uint8_t minorLinkerVersion;
    ///< Sum of sizes of sections with code
    uint32_t sizeOfCode;
    ///< Sum of sizes of sections with initialized data
    uint32_t sizeOfInitializedData;
    ///< Sum of sizes of sections with uninitialized data
    uint32_t sizeOfUninitializedData;
    ///< RVA pointer for program launching
    uint32_t addressOfEntryPoint;
    ///< RVA pointer to code of program
    uint32_t baseOfCode;
    ///< Base address for program
    uint64_t imageBase;
    ///< Allignment for section while loading in memory (in bytes)
    uint32_t sectionAlignment;
    ///< Allignment for section in the file (in bytes)
    uint32_t fileAlignment;
    uint16_t majorOperatingSystemVersion;
    uint16_t minorOperatingSystemVersion;
    uint16_t majorImageVersion;
    uint16_t minorImageVersion;
    uint16_t majorSubsystemVersion;
    uint16_t minorSubsystemVersion;
    /// Reserved (always 0)
    uint32_t win32VersionValue;
    ///< Size of the file in memory with headers
    uint32_t sizeOfImage;
    ///< Sum of sizes of headers and DOS-stub
    uint32_t sizeOfHeaders;
    ///< Control sum of the file
    uint32_t checkSum;
    ///< Windows subsystem for execution
    uint16_t subsystem;
    ///< Addional attributes of the file
    uint16_t dllCharacteristics;
    ///< Size of started stack in virtual memory
    uint64_t sizeOfStackReserve;
    ///< Size of started stack
    uint64_t sizeOfStackCommit;
    ///< Size of heap
    uint64_t sizeOfHeapReserve;
    ///< Size of stated heap
    uint64_t sizeOfHeapCommit;
    ///< Deprecated
    uint32_t loaderFlags;
    ///< Number of data directories (always 16)
    uint32_t numberOfRvaAndSizes;
    struct DataDirectory dataDirectory[16];

    /// @}
};

struct
#ifdef _MSC_VER
#pragma packed()
#else
    __attribute__((packed))
#endif
    SectionHeader
{
    /// @name Structure of a section header

    /// @{

    ///< Name of the section
    char name[8];
    ///< Size of the section in memory
    uint32_t virtualSize;
    ///< RVA pointer to the section
    uint32_t virtualAddress;
    ///< Size of the section in the file
    uint32_t sizeOfRawData;
    ///< Raw pointer to data of the section
    uint32_t pointerToRawData;
    uint32_t pointerToRelocations;
    uint32_t pointerToLinenumbers;
    uint16_t numberOfRelocations;
    uint16_t numberOfLinenumbers;
    ///< Attributes of the section
    uint32_t characteristics;

    /// @}
};

struct PEFile
{
    /// @name File headers
    ///@{

    /// Main header
    struct PEHeader header;
    /// Optional header
    struct OptionalHeader optionalHeader;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *sectionHeaders;
    ///@}
};

/// @brief Allocate memory for section headers
/// @param[in] count Size of an array of section headers
/// @return Pointer to an allocated memory for an array of section headers (can be NULL if allocation got fault)
struct SectionHeader *allocateSectionHeaders(size_t count);

/// @brief Free allocated memory from PEFile
/// @param[in] file File that need to be free
void freePEFile(struct PEFile file);

#endif
