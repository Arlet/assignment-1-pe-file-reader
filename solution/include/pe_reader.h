#ifndef PE_READER_H
#define PE_READER_H

/// @file
/// @brief Header file of pe_reader

#include "pe_format.h"
#include "stdio.h"

/// @brief Result of readPEFile()
enum readStatus
{
    READ_SUCCESS,           ///< Read is success
    FILE_READ_ERROR,        ///< When got error from file reading
    WRONG_SIGNATURE,        ///< File has not got PE signature
    MEMORY_ALLOCATION_ERROR ///< Memory for section headers didn't allocate
};

/// @brief Read PE headers
/// @param[in] file PE File that need to read
/// @param[out] peFile pointer to variable for save result
/// @return some read status (more in readStatus)
enum readStatus readPEFile(FILE *file, struct PEFile *peFile);

#endif
