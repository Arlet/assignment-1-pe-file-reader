#ifndef SECTION_WRITER_H
#define SECTION_WRITER_H

/// @file
/// @brief Header file of section writer

#include "pe_format.h"
#include "stdio.h"

/// @brief Result of writeSection()
enum writeStatus
{
    WRITE_SUCCESS,     ///< If writing was success
    WRITE_ERROR,       ///< If something got wrong with writing in a file
    SECTION_READ_ERROR ///< If something got wrong with section reading
};

/// @brief Write a section in output file
/// @param[in] readingFile File that use for reading the section
/// @param[in] writingFile File where will write the section
/// @param[in] peFile Prepared PE file for getting address
/// @param[in] sectionIndex Index in peFile of section
enum writeStatus writeSection(FILE *readingFile, FILE *writingFile, struct PEFile peFile, size_t sectionIndex);

#endif
